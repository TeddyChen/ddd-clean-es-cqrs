package ntut.csie.sslab.ddd.entity;

import java.util.List;

public interface DomainEventSource<E extends DomainEvent> {
    void apply(E event);
    void clearDomainEvents();

    List<E> getDomainEvents();
    E getLastDomainEvent();
    int getDomainEventSize();
}
