package ntut.csie.sslab.ezkanban.kanban.mytag.adapter.out.repository.springboot;

import ntut.csie.sslab.ddd.framework.OrmClient;
import ntut.csie.sslab.ezkanban.kanban.mytag.usecase.port.out.repository.MyTagData;

// generate bean for PostgresOutboxStoreClient

public interface MyTagOrmClient extends OrmClient<MyTagData, String> {

}