package ntut.csie.sslab.ezkanban.kanban.mytag.all_stream.ex11;


import ntut.csie.sslab.ddd.usecase.AbstractRepository;
import ntut.csie.sslab.ezkanban.kanban.mytag.type_mapping.ex5.MyTag;

public interface MyTagRepository extends AbstractRepository<MyTag, String> {
}
