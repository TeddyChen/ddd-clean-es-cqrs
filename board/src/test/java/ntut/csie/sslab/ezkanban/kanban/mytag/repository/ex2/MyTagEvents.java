package ntut.csie.sslab.ezkanban.kanban.mytag.repository.ex2;

import ntut.csie.sslab.ddd.entity.DomainEvent;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;

import java.util.Date;
import java.util.UUID;

interface MyTagEvents extends DomainEvent {

    BoardId boardId();
    String tagId();

    default String aggregateId(){
        return boardId().id();
    }

    ///////////////////////////////////////////////////////////////
    record MyTagCreated(
            BoardId boardId,
            String tagId,
            String name,
            String color,
            UUID id,
            Date occurredOn
    ) implements MyTagEvents {}

    record MyTagRenamed(
            BoardId boardId,
            String tagId,
            String name,
            UUID id,
            Date occurredOn
    ) implements MyTagEvents {}

    record MyTagColorChanged(
            BoardId boardId,
            String tagId,
            String color,
            UUID id,
            Date occurredOn
    ) implements MyTagEvents {}

    record MyTagDeleted(
            BoardId boardId,
            String tagId,
            String userId,
            UUID id,
            Date occurredOn
    ) implements MyTagEvents {}
}
