package ntut.csie.sslab.ezkanban.kanban.mytag.repository.ex4;

import ntut.csie.sslab.ddd.usecase.DomainEventMapper;
import ntut.csie.sslab.ddd.usecase.OutboxMapper;
import ntut.csie.sslab.ezkanban.kanban.mytag.usecase.port.out.repository.MyTagData;

import java.util.stream.Collectors;

class MyTagMapper implements OutboxMapper<MyTag, MyTagData> {
    @Override
    public MyTag toDomain(MyTagData data) {
        MyTag mytag = new MyTag(data.getBoardId(), data.getId(), data.getName(), data.getColor());
        mytag.setVersion(data.getVersion());
        mytag.clearDomainEvents();
        return mytag;
    }

    @Override
    public MyTagData toData(MyTag tag) {
        MyTagData data = new MyTagData();
        data.setId(tag.getId());
        data.setBoardId(tag.getBoardId());
        data.setName(tag.getName());
        data.setColor(tag.getColor());
        data.setDomainEventDatas(tag.getDomainEvents().stream().map(DomainEventMapper::toData).collect(Collectors.toList()));
        data.setStreamName(tag.getStreamName());
        data.setVersion(tag.getVersion());
        return data;
    }
}
