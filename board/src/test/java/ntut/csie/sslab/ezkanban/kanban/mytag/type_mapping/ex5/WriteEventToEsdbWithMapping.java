package ntut.csie.sslab.ezkanban.kanban.mytag.type_mapping.ex5;

import com.eventstore.dbclient.*;
import ntut.csie.sslab.ddd.entity.common.DateProvider;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WriteEventToEsdbWithMapping {
    private final static String ESDB_URL = "esdb://127.0.0.1:2113?tls=false";
    private EventStoreDBClient client;

    private String streamName = "MyTag-000000000000a";
    @BeforeEach
    public void setUp(){
        EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(ESDB_URL);
        this.client = EventStoreDBClient.create(settings);
    }

    @Test
    public void step_1_create_a_mytag_created_event_and_write_it_to_esdb_with_its_mapping_name() throws ExecutionException, InterruptedException, IOException {
        MyTagEvents.MyTagCreated mytagCreated =
                new MyTagEvents.MyTagCreated(BoardId.valueOf("board id"), UUID.randomUUID().toString(), "Event Type Without Mapping", "Yellow", UUID.randomUUID(), DateProvider.now());
        EventData event = EventData.builderAsJson(MyTagEvents.mapper().toMappingType(MyTagEvents.MyTagCreated.class) , mytagCreated)
            .build();

        // write the event to stream
        WriteResult writeResult = client
                .appendToStream(streamName, event)
                .get()                ;


        // read the event from stream
        ReadStreamOptions readStreamOptions = ReadStreamOptions.get()
                .fromStart()
                .notResolveLinkTos();
        ReadResult readResult = client
                .readStream(streamName, 1, readStreamOptions)
                .get();
        ResolvedEvent resolvedEvent = readResult
                .getEvents()
                .get(0);

        String eventType = resolvedEvent.getEvent().getEventType();
        Class cls = MyTagEvents.mapper().getMap().get(eventType);
        assertEquals("ntut.csie.sslab.ezkanban.kanban.mytag.type_mapping.ex5.MyTagEvents$MyTagCreated", cls.getTypeName());
    }
    @Test
    public void step_2_rename_mytag_created_to_mytag_added() {
    }

    @Test
    public void step_3_read_the_event_again() throws IOException, ExecutionException, InterruptedException {
        // read the event from stream
        ReadStreamOptions readStreamOptions = ReadStreamOptions.get()
                .fromStart()
                .notResolveLinkTos();
        ReadResult readResult = client
                .readStream(streamName, 1, readStreamOptions)
                .get();
        ResolvedEvent resolvedEvent = readResult
                .getEvents()
                .get(0);

        var mapper = MyTagEvents.mapper();
        String eventType = resolvedEvent.getEvent().getEventType();
        Class cls = MyTagEvents.mapper().getMap().get(eventType);
        assertEquals("ntut.csie.sslab.ezkanban.kanban.mytag.type_mapping.ex5.MyTagEvents$MyTagAdded", cls.getTypeName());
    }

    @Test
    public void step_4_rename_mytag_added_to_mytag_created() {
    }
}
