package ntut.csie.sslab.ezkanban.kanban.mytag.repository.ex2;

import ntut.csie.sslab.ddd.adapter.gateway.GoogleEventBusAdapter;
import ntut.csie.sslab.ddd.usecase.DomainEventBus;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import ntut.csie.sslab.ezkanban.kanban.common.usecase.AllEventsListener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.util.UUID;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.isA;


public class SaveMyTagWithInMemoryRepositoryTest {
    private MyTagRepository mytagRepository;
    private SimpleAsyncTaskExecutor executor;
    private DomainEventBus eventBus;
    private AllEventsListener allEventsListener;

    @BeforeEach
    public void setUp(){
        eventBus = new GoogleEventBusAdapter();
        mytagRepository = new MyTagInMemoryRepository(eventBus);

        allEventsListener = Mockito.mock(AllEventsListener.class);
        eventBus.register(allEventsListener);

        executor = new SimpleAsyncTaskExecutor();
        executor.execute((GoogleEventBusAdapter) eventBus);
    }

    @Test
    public void create_a_mytag(){
        MyTag mytag = new MyTag(BoardId.create(),
                UUID.randomUUID().toString(),
                "bug",
                "Red");
        mytagRepository.save(mytag);

        assertTrue(mytagRepository.findById(mytag.getId()).isPresent());
        MyTag mytagFromDB = mytagRepository.findById(mytag.getId()).get();
        assertEquals(mytag.getTagId(), mytagFromDB.getId());
        assertEquals(mytag.getBoardId(), mytagFromDB.getBoardId());
        assertEquals(mytag.getName(), mytagFromDB.getName());
        assertEquals(mytag.getColor(), mytagFromDB.getColor());
        await().untilAsserted(()-> Mockito.verify(allEventsListener,
                Mockito.times(1)).when(isA(MyTagEvents.MyTagCreated.class)));
    }
}
