package ntut.csie.sslab.ezkanban.kanban.mytag.type_mapping.ex5;

import com.eventstore.dbclient.*;
import ntut.csie.sslab.ddd.entity.common.DateProvider;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class WriteEventToEsdbWithoutMapping {
    private final static String ESDB_URL = "esdb://127.0.0.1:2113?tls=false";
    private EventStoreDBClient client;

    private String streamName = "MyTag-00000000000001";
    @BeforeEach
    public void setUp(){
        EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(ESDB_URL);
        this.client = EventStoreDBClient.create(settings);
    }

    @Test
    public void step_1_create_a_mytag_created_event_and_write_it_to_esdb_with_its_type_name() throws ExecutionException, InterruptedException, IOException {
        MyTagEvents.MyTagCreated mytagCreated =
                new MyTagEvents.MyTagCreated(BoardId.valueOf("board id"), UUID.randomUUID().toString(), "Event Type Without Mapping", "Yellow", UUID.randomUUID(), DateProvider.now());
        EventData event = EventData.builderAsJson(MyTagEvents.MyTagCreated.class.getTypeName(), mytagCreated)
            .build();

        // write the event to stream
        WriteResult writeResult = client
                .appendToStream(streamName, event)
                .get()                ;


        // read the event from stream
        ReadStreamOptions readStreamOptions = ReadStreamOptions.get()
                .fromStart()
                .notResolveLinkTos();
        ReadResult readResult = client
                .readStream(streamName, 1, readStreamOptions)
                .get();
        ResolvedEvent resolvedEvent = readResult
                .getEvents()
                .get(0);

        try {
            String eventType = resolvedEvent.getEvent().getEventType();
            Class cls = Class.forName(eventType);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void step_2_rename_mytag_created_to_mytag_added() {
    }

    @Test
    public void step_3_read_the_event_again() throws ExecutionException, InterruptedException {
        // read the event from stream
        ReadStreamOptions readStreamOptions = ReadStreamOptions.get()
                .fromStart()
                .notResolveLinkTos();
        ReadResult readResult = client
                .readStream(streamName, 1, readStreamOptions)
                .get();
        ResolvedEvent resolvedEvent = readResult
                .getEvents()
                .get(0);

        try {
            String eventType = resolvedEvent.getEvent().getEventType();
            Class cls = Class.forName(eventType);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void step_4_rename_mytag_added_to_mytag_created() {
    }
}
