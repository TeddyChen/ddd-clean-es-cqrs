package ntut.csie.sslab.ezkanban.kanban.mytag.optimistic_locking.ex8;

import ntut.csie.sslab.ddd.entity.AggregateRoot;
import ntut.csie.sslab.ddd.entity.DomainEvent;
import ntut.csie.sslab.ddd.entity.common.DateProvider;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;

import java.util.List;
import java.util.UUID;

public class MyTag extends AggregateRoot<String, MyTagEvents> {

    public final static String CATEGORY = "MyTag";
    private BoardId boardId;
    private String name;
    private String color;

    public MyTag(BoardId boardId, String tagId, String name, String color) {
        super(tagId);
        apply(new MyTagEvents.MyTagCreated(boardId, tagId, name, color, UUID.randomUUID(), DateProvider.now()));
    }

    public MyTag(List<DomainEvent> events) {
        super(events);
    }

    public void rename(String newName) {
        apply(new MyTagEvents.MyTagRenamed(boardId, id, newName, UUID.randomUUID(), DateProvider.now()));
    }

    @Override
    public void markAsDeleted(String userId) {
        apply(new MyTagEvents.MyTagDeleted(boardId, id, userId, UUID.randomUUID(), DateProvider.now()));
    }

    public BoardId getBoardId() {
        return boardId;
    }

    public String getTagId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String getCategory() {
        return CATEGORY;
    }

    @Override
    protected void when(DomainEvent event) {
        switch (event){
            case MyTagEvents.MyTagCreated e -> {
                this.id = e.tagId();
                this.boardId = e.boardId();
                this.name = e.name();
                this.color = e.color();
                this.isDeleted = false;
            }
            case MyTagEvents.MyTagRenamed e -> {
                this.name = e.name();
            }
            case MyTagEvents.MyTagColorChanged e -> {
                this.color = e.color();
            }
            case MyTagEvents.MyTagDeleted e -> {
                this.isDeleted = true;
            }
            default -> throw new RuntimeException("Unsupported event type: " + event.getClass());
        }
    }
}
