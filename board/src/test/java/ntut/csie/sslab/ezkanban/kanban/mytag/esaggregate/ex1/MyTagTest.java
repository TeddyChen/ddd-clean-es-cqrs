package ntut.csie.sslab.ezkanban.kanban.mytag.esaggregate.ex1;

import ntut.csie.sslab.ddd.entity.common.DateProvider;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class MyTagTest {
    @Test
    public void create_a_mytag_generates_a_mytag_created_event(){
        BoardId boardId = BoardId.create();
        String tagId = UUID.randomUUID().toString();

        MyTag mytag = new MyTag(boardId, tagId, "Bug", "Red");

        assertEquals(tagId, mytag.getId());
        assertEquals(boardId, mytag.getBoardId());
        assertEquals("Bug", mytag.getName());
        assertEquals("Red", mytag.getColor());
        assertFalse(mytag.isDeleted());
        assertEquals(1, mytag.getDomainEvents().size());
        assertEquals(MyTagEvents.MyTagCreated.class, mytag.getDomainEvents().get(0).getClass());
    }


    @Test
    public void rename_a_tag(){
        BoardId boardId = BoardId.create();
        String tagId = UUID.randomUUID().toString();
        MyTag mytag = new MyTag(boardId, tagId, "Feature", "White");

        mytag.rename("Issue");

        assertEquals(tagId, mytag.getId());
        assertEquals(boardId, mytag.getBoardId());
        assertEquals("Issue", mytag.getName());
        assertEquals("White", mytag.getColor());
        assertEquals(2, mytag.getDomainEvents().size());
        assertEquals(MyTagEvents.MyTagCreated.class, mytag.getDomainEvents().get(0).getClass());
        assertEquals(MyTagEvents.MyTagRenamed.class, mytag.getDomainEvents().get(1).getClass());
    }

    @Test
    public void delete_mytag(){
        BoardId boardId = BoardId.create();
        String tagId = UUID.randomUUID().toString();
        MyTag mytag = new MyTag(boardId, tagId, "Bug", "Black");

        mytag.markAsDeleted("teddy");

        assertTrue(mytag.isDeleted());
        assertEquals(2, mytag.getDomainEvents().size());
        assertEquals(MyTagEvents.MyTagCreated.class, mytag.getDomainEvents().get(0).getClass());
        assertEquals(MyTagEvents.MyTagDeleted.class, mytag.getDomainEvents().get(1).getClass());
    }

    @Test
    public void restore_mytag_state_with_events(){

        // TODO: Fix MyTag class with the event sourced coding style to pass this test method
        BoardId boardId = BoardId.create();
        String mytagId = UUID.randomUUID().toString();
        MyTagEvents.MyTagCreated myTagCreated =
                new MyTagEvents.MyTagCreated(boardId, mytagId, "Feature", "Yellow", UUID.randomUUID(), DateProvider.now());
        MyTagEvents.MyTagRenamed mytagRename =
                new MyTagEvents.MyTagRenamed(boardId, mytagId, "Bug", UUID.randomUUID(), DateProvider.now());

        MyTag mytag = new MyTag(Arrays.asList(myTagCreated, mytagRename));

        assertEquals(mytagId, mytag.getId());
        assertEquals(boardId, mytag.getBoardId());
        assertEquals("Bug", mytag.getName());
        assertEquals("Yellow", mytag.getColor());
        assertEquals(0, mytag.getDomainEvents().size());
    }
}
