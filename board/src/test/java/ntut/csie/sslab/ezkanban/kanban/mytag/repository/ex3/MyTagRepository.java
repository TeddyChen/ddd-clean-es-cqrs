package ntut.csie.sslab.ezkanban.kanban.mytag.repository.ex3;


import ntut.csie.sslab.ddd.usecase.AbstractRepository;
interface MyTagRepository extends AbstractRepository<MyTag, String> {
}
