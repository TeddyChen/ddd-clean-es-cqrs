package ntut.csie.sslab.ezkanban.kanban.mytag.find_mytag_by_board_id.ex6;


import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;

import java.util.List;

interface FindMyTagsByBoardIdInquiry {
    List<MyTag> query(BoardId boardId);
}
