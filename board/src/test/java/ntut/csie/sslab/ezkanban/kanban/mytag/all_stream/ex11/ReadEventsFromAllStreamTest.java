package ntut.csie.sslab.ezkanban.kanban.mytag.all_stream.ex11;

import com.eventstore.dbclient.*;
import ntut.csie.sslab.ddd.entity.common.DateProvider;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReadEventsFromAllStreamTest {
    private final static String ESDB_URL = "esdb://127.0.0.1:2113?tls=false";
    private EventStoreDBClient client;

    private String streamName = "$all";
    @BeforeEach
    public void setUp(){
        EventStoreDBClientSettings settings = EventStoreDBConnectionString.parseOrThrow(ESDB_URL);
        this.client = EventStoreDBClient.create(settings);
    }

    /*
    See also: EsdbPersistentListener
     */
    @Test
    public void read_events_from_all_stream() throws ExecutionException, InterruptedException {

        ReadStreamOptions readStreamOptions = ReadStreamOptions.get()
                .fromStart()
                .notResolveLinkTos();

        ReadResult readResult = client.readAll(200).get();

        readResult.getEvents().stream().forEach( x-> System.out.println("Event Type: "  + x.getEvent().getEventType()));
    }

}
