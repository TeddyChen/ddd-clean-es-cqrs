package ntut.csie.sslab.ezkanban.kanban.mytag.repository.ex4;

import ntut.csie.sslab.ddd.adapter.eventbroker.PostgresDomainEventListener;
import ntut.csie.sslab.ddd.adapter.gateway.GoogleEventBusAdapter;
import ntut.csie.sslab.ddd.adapter.repository.EzOutboxStoreAdapter;
import ntut.csie.sslab.ddd.framework.EzOutboxStore;
import ntut.csie.sslab.ddd.framework.ezes.PgMessageDbClient;
import ntut.csie.sslab.ddd.usecase.DomainEventMapper;
import ntut.csie.sslab.ddd.usecase.MessageDataMapper;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import ntut.csie.sslab.ezkanban.kanban.common.usecase.AbstractSpringBootJpaTest;
import ntut.csie.sslab.ezkanban.kanban.common.usecase.AllEventsListener;
import ntut.csie.sslab.ezkanban.kanban.mytag.adapter.out.repository.springboot.MyTagOrmClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.sql.SQLException;
import java.util.UUID;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.isA;


public class SaveMyTagWithOutboxRepositoryTest extends AbstractSpringBootJpaTest {


    @Autowired
    @Qualifier("MyTagPostgresOutboxStoreClient")
    private EzOutboxStore mytagEzOutboxStore;

    private MyTagRepository mytagRepository;

    private SimpleAsyncTaskExecutor executor;
    private GoogleEventBusAdapter eventBus;
    private AllEventsListener allEventsListener;

    @Autowired
    private MyTagOrmClient mytagOrmStoreClient;

    @Autowired
    private PgMessageDbClient pgMessageDbClient;

    @BeforeEach
    public void setUp(){
        MessageDataMapper.setMapper(MyTagEvents.mapper());
        DomainEventMapper.setMapper(MyTagEvents.mapper());

        mytagRepository = new MyTagOutboxRepository(new EzOutboxStoreAdapter(
                new EzOutboxStore(mytagOrmStoreClient, pgMessageDbClient)));

        eventBus = new GoogleEventBusAdapter();
        executor = new SimpleAsyncTaskExecutor();
        executor.execute(eventBus);

        try {
            postgresDomainEventListener = new PostgresDomainEventListener(JDBC_TEST_URL, "postgres", "root", 20, MyTagEvents.mapper(), eventBus);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        executor.execute(postgresDomainEventListener);

        allEventsListener = Mockito.mock(AllEventsListener.class);
        eventBus.register(allEventsListener);
    }

    @Test
    public void create_a_mytag_and_save_it_to_outbox_repository(){
        // TODO: open http://127.0.0.1:5050/browser/ to see the written data in the mytag table
        //  and the event in the messages table after executing this test method

        MyTag mytag = new MyTag(BoardId.create(),
                UUID.randomUUID().toString(),
                "bug",
                "Red");
        mytagRepository.save(mytag);
        await().untilAsserted(()-> Mockito.verify(allEventsListener,
                Mockito.times(1)).when(isA(MyTagEvents.MyTagCreated.class)));

        assertTrue(mytagRepository.findById(mytag.getId()).isPresent());
        MyTag mytagFromDB = mytagRepository.findById(mytag.getId()).get();
        assertEquals(mytag.getTagId(), mytagFromDB.getId());
        assertEquals(mytag.getBoardId(), mytagFromDB.getBoardId());
        assertEquals(mytag.getName(), mytagFromDB.getName());
        assertEquals(mytag.getColor(), mytagFromDB.getColor());
    }
}
