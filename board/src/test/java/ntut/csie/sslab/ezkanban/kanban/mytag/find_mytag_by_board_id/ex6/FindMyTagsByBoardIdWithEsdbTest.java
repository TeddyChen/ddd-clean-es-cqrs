package ntut.csie.sslab.ezkanban.kanban.mytag.find_mytag_by_board_id.ex6;

import ntut.csie.sslab.ddd.adapter.eventbroker.EsdbListener;
import ntut.csie.sslab.ddd.adapter.eventbroker.EsdbPersistentListener;
import ntut.csie.sslab.ddd.adapter.gateway.GoogleEventBusAdapter;
import ntut.csie.sslab.ddd.adapter.repository.EsdbStoreAdapter;
import ntut.csie.sslab.ddd.usecase.DomainEventMapper;
import ntut.csie.sslab.ddd.usecase.MessageDataMapper;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import ntut.csie.sslab.ezkanban.kanban.common.usecase.AllEventsListener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.util.UUID;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;


public class FindMyTagsByBoardIdWithEsdbTest {

    private final static String ESDB_URL = "esdb://127.0.0.1:2113?tls=false";
    private MyTagRepository mytagRepository;

    private SimpleAsyncTaskExecutor executor;
    private GoogleEventBusAdapter eventBus;
    private AllEventsListener allEventsListener;
    public EsdbListener esdbListener;

    private FindMyTagsByBoardIdInquiry esdbFindMyTagsByBoardIdInquiry;

    @BeforeEach
    public void setUp(){
        MessageDataMapper.setMapper(MyTagEvents.mapper());
        DomainEventMapper.setMapper(MyTagEvents.mapper());

        eventBus = new GoogleEventBusAdapter();

        // EventStoreDB
        mytagRepository = new MyTagEventSourcingRepository(new EsdbStoreAdapter(ESDB_URL));
        esdbFindMyTagsByBoardIdInquiry = new EsdbFindMyTagsByBoardIdInquiry(new EsdbStoreAdapter(ESDB_URL));
        esdbListener = new EsdbPersistentListener(ESDB_URL, MyTagEvents.mapper(), eventBus);

        executor = new SimpleAsyncTaskExecutor();
        executor.execute(esdbListener);
        executor.execute(eventBus);

        allEventsListener = Mockito.mock(AllEventsListener.class);
        eventBus.register(allEventsListener);
    }

    @Test
    public void find_all_mytags_in_a_board(){

        BoardId boardId1 = BoardId.create();
        String tag1Id = createMyTagInBoard(boardId1, "bug", "red");
        String tag2Id = createMyTagInBoard(boardId1, "feature", "white");
        String tag3Id = createMyTagInBoard(boardId1, "issue", "black");

        BoardId boardId2 = BoardId.create();
        String tag4Id = createMyTagInBoard(boardId2, "bug", "blue");

        BoardId boardId3 = BoardId.create();
        String tag5Id = createMyTagInBoard(boardId3, "bug", "green");
        String tag6Id = createMyTagInBoard(boardId3, "problem", "black");
        await().untilAsserted(()-> Mockito.verify(allEventsListener,
                Mockito.times(6)).when(isA(MyTagEvents.MyTagCreated.class)));

        assertEquals(3, esdbFindMyTagsByBoardIdInquiry.query(boardId1).size());
        assertEquals(1, esdbFindMyTagsByBoardIdInquiry.query(boardId2).size());
        assertEquals(2, esdbFindMyTagsByBoardIdInquiry.query(boardId3).size());
    }

    private String createMyTagInBoard(BoardId boardId, String name, String color){
        MyTag mytag = new MyTag(boardId,
                UUID.randomUUID().toString(),
                name,
                color);
        mytagRepository.save(mytag);

        return mytag.getTagId();
    }
}
