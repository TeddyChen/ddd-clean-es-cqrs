package ntut.csie.sslab.ezkanban.kanban.mytag.repository.ex3;

import ntut.csie.sslab.ddd.adapter.eventbroker.EsdbListener;
import ntut.csie.sslab.ddd.adapter.eventbroker.EsdbPersistentListener;
import ntut.csie.sslab.ddd.adapter.gateway.GoogleEventBusAdapter;
import ntut.csie.sslab.ddd.adapter.repository.EsdbStoreAdapter;
import ntut.csie.sslab.ddd.usecase.DomainEventMapper;
import ntut.csie.sslab.ddd.usecase.MessageDataMapper;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import ntut.csie.sslab.ezkanban.kanban.common.usecase.AllEventsListener;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.util.UUID;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.isA;

public class SaveMyTagWithEsdbRepositoryTest {

    private final static String ESDB_URL = "esdb://127.0.0.1:2113?tls=false";
    private MyTagRepository mytagRepository;

    private SimpleAsyncTaskExecutor executor;
    private GoogleEventBusAdapter eventBus;
    private AllEventsListener allEventsListener;
    public EsdbListener esdbListener;

    @BeforeEach
    public void setUp(){
        MessageDataMapper.setMapper(MyTagEvents.mapper());
        DomainEventMapper.setMapper(MyTagEvents.mapper());

        eventBus = new GoogleEventBusAdapter();

        // EventStoreDB
        mytagRepository = new MyTagEventSourcingRepository(new EsdbStoreAdapter(ESDB_URL));
        esdbListener = new EsdbPersistentListener(ESDB_URL, MyTagEvents.mapper(), eventBus);

        executor = new SimpleAsyncTaskExecutor();
        executor.execute(esdbListener);
        executor.execute(eventBus);

        allEventsListener = Mockito.mock(AllEventsListener.class);
        eventBus.register(allEventsListener);
    }

    @AfterEach
    public void teardown(){
        ((EsdbPersistentListener)esdbListener).deletePersistentSubscription();
        esdbListener.shutdown();
    }

    @Test
    public void create_a_mytag_and_save_it_to_esdb_repository(){
   // TODO: open http://localhost:2113/web/index.html#/streams to see the written event after executing this test method

        MyTag mytag = new MyTag(BoardId.create(),
                UUID.randomUUID().toString(),
                "bug",
                "Red");
        mytagRepository.save(mytag);
        await().untilAsserted(()-> Mockito.verify(allEventsListener,
                Mockito.times(1)).when(isA(MyTagEvents.MyTagCreated.class)));

        assertTrue(mytagRepository.findById(mytag.getId()).isPresent());
        MyTag mytagFromDB = mytagRepository.findById(mytag.getId()).get();
        assertEquals(mytag.getTagId(), mytagFromDB.getId());
        assertEquals(mytag.getBoardId(), mytagFromDB.getBoardId());
        assertEquals(mytag.getName(), mytagFromDB.getName());
        assertEquals(mytag.getColor(), mytagFromDB.getColor());
    }

}
