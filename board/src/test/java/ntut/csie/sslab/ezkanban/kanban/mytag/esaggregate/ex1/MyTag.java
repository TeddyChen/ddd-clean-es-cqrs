package ntut.csie.sslab.ezkanban.kanban.mytag.esaggregate.ex1;

import ntut.csie.sslab.ddd.entity.AggregateRoot;
import ntut.csie.sslab.ddd.entity.DomainEvent;
import ntut.csie.sslab.ddd.entity.common.DateProvider;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;

import java.util.List;
import java.util.UUID;

public class MyTag extends AggregateRoot<String, MyTagEvents> {

    public final static String CATEGORY = "MyTag";
    private BoardId boardId;
    private String name;
    private String color;

    public MyTag(BoardId boardId, String mytagId, String name, String color) {
        super(mytagId);
        this.boardId = boardId;
        this.name = name;
        this.color = color;
        addDomainEvent(new MyTagEvents.MyTagCreated(
                boardId,
                mytagId,
                name,
                color,
                UUID.randomUUID(),
                DateProvider.now()));
    }

    public MyTag(List<DomainEvent> events) {
        super(events);
    }

    public void rename(String newName) {
        this.name = newName;
        addDomainEvent(new MyTagEvents.MyTagRenamed(
                boardId,
                id,
                newName,
                UUID.randomUUID(),
                DateProvider.now()));
    }

    @Override
    public void markAsDeleted(String userId) {
        this.isDeleted = true;
        addDomainEvent(new MyTagEvents.MyTagDeleted(
                boardId,
                id, userId,
                UUID.randomUUID(),
                DateProvider.now()));
    }

    public BoardId getBoardId() {
        return boardId;
    }

    public String getTagId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String getCategory() {
        return CATEGORY;
    }

    @Override
    protected void when(DomainEvent domainEvent) {

    }

}
