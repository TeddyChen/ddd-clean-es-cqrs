package ntut.csie.sslab.ezkanban.kanban.mytag.optimistic_locking.ex8;

import ntut.csie.sslab.ddd.adapter.eventbroker.EsdbListener;
import ntut.csie.sslab.ddd.adapter.eventbroker.EsdbPersistentListener;
import ntut.csie.sslab.ddd.adapter.gateway.GoogleEventBusAdapter;
import ntut.csie.sslab.ddd.adapter.repository.EsdbStoreAdapter;
import ntut.csie.sslab.ddd.usecase.DomainEventMapper;
import ntut.csie.sslab.ddd.usecase.MessageDataMapper;
import ntut.csie.sslab.ddd.usecase.RepositorySaveException;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import ntut.csie.sslab.ezkanban.kanban.common.usecase.AllEventsListener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class OptimisticLockingWithEsdbTest {

    private final static String ESDB_URL = "esdb://127.0.0.1:2113?tls=false";
    private MyTagRepository mytagRepository;

    private SimpleAsyncTaskExecutor executor;
    private GoogleEventBusAdapter eventBus;
    private AllEventsListener allEventsListener;
    public EsdbListener esdbListener;

    @BeforeEach
    public void setUp(){
        MessageDataMapper.setMapper(MyTagEvents.mapper());
        DomainEventMapper.setMapper(MyTagEvents.mapper());

        eventBus = new GoogleEventBusAdapter();

        // EventStoreDB
        mytagRepository = new MyTagEventSourcingRepository(new EsdbStoreAdapter(ESDB_URL));
        esdbListener = new EsdbPersistentListener(ESDB_URL, MyTagEvents.mapper(), eventBus);

        executor = new SimpleAsyncTaskExecutor();
        executor.execute(esdbListener);
        executor.execute(eventBus);

        allEventsListener = Mockito.mock(AllEventsListener.class);
        eventBus.register(allEventsListener);
    }

    @Test
    public void optimistic_locking_failure_with_esdb() {
        MyTag mytag = new MyTag(BoardId.create(),
                UUID.randomUUID().toString(),
                "feature",
                "white");
        mytagRepository.save(mytag);

        MyTag mytagV1 = mytagRepository.findById(mytag.getTagId()).get();
        MyTag mytagV2 = mytagRepository.findById(mytag.getTagId()).get();
        mytagV1.rename("story");
        mytagRepository.save(mytagV1);

        try {
            mytagRepository.save(mytagV2);
            fail("Infeasible path");
        } catch (RepositorySaveException e) {
            assertEquals("Optimistic locking failure", e.getMessage());
        }
    }
}
