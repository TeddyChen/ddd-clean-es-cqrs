package ntut.csie.sslab.ezkanban.kanban.mytag.repository.ex4;


import ntut.csie.sslab.ddd.usecase.AbstractRepository;

interface MyTagRepository extends AbstractRepository<MyTag, String> {
}
