package ntut.csie.sslab.ezkanban.kanban.mytag.repository.ex4;

import ntut.csie.sslab.ddd.usecase.GenericOutboxRepository;
import ntut.csie.sslab.ddd.usecase.OutboxStore;
import ntut.csie.sslab.ezkanban.kanban.mytag.usecase.port.out.repository.MyTagData;
import java.util.Optional;

class MyTagOutboxRepository implements MyTagRepository {
    private final GenericOutboxRepository<MyTag, MyTagData, String> outboxRepository;
    private final OutboxStore<MyTagData, String> store;

    public MyTagOutboxRepository(OutboxStore<MyTagData, String> store) {
        this.outboxRepository = new GenericOutboxRepository<>(store, new MyTagMapper());
        this.store = store;
    }

    @Override
    public Optional<MyTag> findById(String mytagId) {
        return outboxRepository.findById(mytagId);
    }

    @Override
    public void save(MyTag mytag) {
        outboxRepository.save(mytag);
    }

    @Override
    public void delete(MyTag mytag) {
        outboxRepository.delete(mytag);
    }

}
