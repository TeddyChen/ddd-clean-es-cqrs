package ntut.csie.sslab.ezkanban.kanban.mytag.find_mytag_by_board_id.ex6;


import ntut.csie.sslab.ddd.usecase.AbstractRepository;

interface MyTagRepository extends AbstractRepository<MyTag, String> {
}
