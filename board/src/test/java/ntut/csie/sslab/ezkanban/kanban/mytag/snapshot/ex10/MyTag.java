package ntut.csie.sslab.ezkanban.kanban.mytag.snapshot.ex10;

import ntut.csie.sslab.ddd.entity.AggregateRoot;
import ntut.csie.sslab.ddd.entity.AggregateSnapshot;
import ntut.csie.sslab.ddd.entity.DomainEvent;
import ntut.csie.sslab.ddd.entity.common.DateProvider;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import ntut.csie.sslab.ezkanban.kanban.tag.entity.Tag;
import ntut.csie.sslab.ezkanban.kanban.tag.entity.TagEvents;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class MyTag extends AggregateRoot<String, MyTagEvents> implements AggregateSnapshot<MyTag.MyTagSnapshot>  {

    public final static String CATEGORY = "MyTag";
    private BoardId boardId;
    private String name;
    private String color;

    //region: Memento (Snapshot)
    public record MyTagSnapshot(BoardId boardId, String tagId, String name, String color, AtomicLong version){}

    public static MyTag fromSnapshot(MyTag.MyTagSnapshot snapshot){
        MyTag mytag = new MyTag();
        mytag.setSnapshot(snapshot);
        return mytag;
    }

    @Override
    public MyTag.MyTagSnapshot getSnapshot() {
        return new MyTag.MyTagSnapshot(boardId, id, name, color, version);
    }

    @Override
    public void setSnapshot(MyTag.MyTagSnapshot snapshot) {
        this.boardId = snapshot.boardId;
        this.id = snapshot.tagId;
        this.name = snapshot.name;
        this.color = snapshot.color;
        this.version = snapshot.version;
    }
    //endregion: Memento (Snapshot)

    protected MyTag(){}

    public MyTag(BoardId boardId, String tagId, String name, String color) {
        super(tagId);
        apply(new MyTagEvents.MyTagCreated(boardId, tagId, name, color, UUID.randomUUID(), DateProvider.now()));
    }

    public MyTag(List<DomainEvent> events) {
        super(events);
    }

    public void rename(String newName) {
        apply(new MyTagEvents.MyTagRenamed(boardId, id, newName, UUID.randomUUID(), DateProvider.now()));
    }

    public void changeColor(String color) {
        if (this.color.equals(color)){
            return;
        }
        apply(new MyTagEvents.MyTagColorChanged(boardId, id, color, UUID.randomUUID(), DateProvider.now()));
    }
    @Override
    public void markAsDeleted(String userId) {
        apply(new MyTagEvents.MyTagDeleted(boardId, id, userId, UUID.randomUUID(), DateProvider.now()));
    }

    public BoardId getBoardId() {
        return boardId;
    }

    public String getTagId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String getCategory() {
        return CATEGORY;
    }

    @Override
    protected void when(DomainEvent event) {
        switch (event){
            case MyTagEvents.MyTagCreated e -> {
                this.id = e.tagId();
                this.boardId = e.boardId();
                this.name = e.name();
                this.color = e.color();
                this.isDeleted = false;
            }
            case MyTagEvents.MyTagRenamed e -> {
                this.name = e.name();
            }
            case MyTagEvents.MyTagColorChanged e -> {
                this.color = e.color();
            }
            case MyTagEvents.MyTagDeleted e -> {
                this.isDeleted = true;
            }
            default -> throw new RuntimeException("Unsupported event type: " + event.getClass());
        }
    }
}
