package ntut.csie.sslab.ezkanban.kanban.mytag.type_mapping.ex5;


import ntut.csie.sslab.ddd.usecase.AbstractRepository;

public interface MyTagRepository extends AbstractRepository<MyTag, String> {
}
