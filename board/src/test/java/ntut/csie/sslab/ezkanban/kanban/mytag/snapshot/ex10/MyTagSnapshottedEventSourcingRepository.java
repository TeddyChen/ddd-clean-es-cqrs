package ntut.csie.sslab.ezkanban.kanban.mytag.snapshot.ex10;

import ntut.csie.sslab.ddd.entity.DomainEvent;
import ntut.csie.sslab.ddd.entity.common.DateProvider;
import ntut.csie.sslab.ddd.entity.common.Json;
import ntut.csie.sslab.ddd.usecase.AggregateRootData;
import ntut.csie.sslab.ddd.usecase.DomainEventData;
import ntut.csie.sslab.ddd.usecase.DomainEventMapper;
import ntut.csie.sslab.ddd.usecase.EventStore;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static ntut.csie.sslab.ddd.entity.common.Contract.requireNotNull;

public class MyTagSnapshottedEventSourcingRepository implements MyTagRepository {

    private final EventStore eventStore;
    private final MyTagEventSourcingRepository mytagEventSourcingRepository;

    private int snapshotIncrement = 5;

    public MyTagSnapshottedEventSourcingRepository(MyTagEventSourcingRepository mytagEventSourcingRepository, EventStore eventStore) {
        requireNotNull("TagEventSourcingRepository", mytagEventSourcingRepository);
        requireNotNull("EventSourcingStore", eventStore);

        this.mytagEventSourcingRepository = mytagEventSourcingRepository;
        this.eventStore = eventStore;
    }

    @Override
    public Optional<MyTag> findById(String mytagId) {

        Optional<DomainEventData> domainEventData =
                eventStore.getLastEventFromStream(getSnapshottedStreamName(mytagId));

        if (domainEventData.isEmpty()){
            return mytagEventSourcingRepository.findById(mytagId);
        }

        DomainEvent.Snapshotted snapshotted = DomainEventMapper.toDomain(domainEventData.get());
        MyTag mytag = MyTag.fromSnapshot(Json.readAs(snapshotted.snapshot().getBytes(), MyTag.MyTagSnapshot.class));
        var events = DomainEventMapper.toDomain(
                eventStore.getEventFromStream(mytag.getStreamName(), mytag.getVersion()+1));
        events.forEach( x -> mytag.apply(x));
        return Optional.of(mytag);
    }


    @Override
    public void save(MyTag mytag) {

        mytagEventSourcingRepository.save(mytag);

        Optional<DomainEventData> snapshotEventData =
                eventStore.getLastEventFromStream(getSnapshottedStreamName(mytag.getId()));
        if (snapshotEventData.isPresent()){
            DomainEvent.Snapshotted snapshotted = DomainEventMapper.toDomain(snapshotEventData.get());
            if (mytag.getVersion() - snapshotted.version() >= snapshotIncrement){
                saveSnapshot(mytag);
            }
        }
        else if (mytag.getVersion() >= snapshotIncrement){
            saveSnapshot(mytag);
        }
    }

    private void saveSnapshot (MyTag mytag){
        MyTag.MyTagSnapshot snapshot = mytag.getSnapshot();
        AggregateRootData data = new AggregateRootData();
        data.setVersion(-1);
        data.setStreamName(getSnapshottedStreamName(mytag.getId()));
        var snapshotted = new DomainEvent.Snapshotted(mytag.getId(), mytag.getCategory(),
                Json.asString(snapshot), mytag.getVersion(), UUID.randomUUID(), DateProvider.now());
        data.setDomainEventDatas(Arrays.asList(DomainEventMapper.toData(snapshotted)));
        eventStore.save(data);
    }

    private String getSnapshottedStreamName(String mytagId){
        return "Snapshot-MyTag-" + mytagId;
    }

    @Override
    public void delete(MyTag tag) {
        mytagEventSourcingRepository.delete(tag);
    }

}
