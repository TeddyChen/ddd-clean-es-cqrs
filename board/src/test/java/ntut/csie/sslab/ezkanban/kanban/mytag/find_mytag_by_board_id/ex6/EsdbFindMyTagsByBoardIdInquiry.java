package ntut.csie.sslab.ezkanban.kanban.mytag.find_mytag_by_board_id.ex6;


import ntut.csie.sslab.ddd.usecase.DomainEventMapper;
import ntut.csie.sslab.ddd.usecase.EventStore;
import ntut.csie.sslab.ddd.usecase.GenericEventSourcingRepository;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class EsdbFindMyTagsByBoardIdInquiry implements FindMyTagsByBoardIdInquiry {
    private final GenericEventSourcingRepository<MyTag> esRepository;
    private final EventStore eventStore;

    public EsdbFindMyTagsByBoardIdInquiry(EventStore eventStore) {
        this.esRepository = new GenericEventSourcingRepository<>(eventStore, MyTag.class, MyTag.CATEGORY);
        this.eventStore = eventStore;
    }

    @Override
    public List<MyTag> query(BoardId boardId){
        List<MyTagEvents.MyTagCreated> mytagCreateds = eventStore.getEventsFromType(MyTagEvents.TypeMapper.MYTAG_CREATED)
                .stream().map(x -> (MyTagEvents.MyTagCreated) DomainEventMapper.toDomain(x)).
                filter( x-> x.boardId().equals(boardId)).toList();

        List<MyTag> result = new ArrayList<>();
        for(var event : mytagCreateds){
            Optional<MyTag> mytag = esRepository.findById(event.tagId());
            if (mytag.isPresent()){
                result.add(mytag.get());
            }
        }
        return result;
    }
}
