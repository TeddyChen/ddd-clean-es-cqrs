package ntut.csie.sslab.ezkanban.kanban.mytag.optimistic_locking.ex7;

import ntut.csie.sslab.ddd.usecase.DomainEventBus;
import ntut.csie.sslab.ddd.usecase.RepositorySaveException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static ntut.csie.sslab.ddd.entity.common.Contract.requireNotNull;

class MyTagInMemoryRepository implements MyTagRepository {
    private final Map<String, MyTag> store = new HashMap();
    private final DomainEventBus domainEventBus;

    public MyTagInMemoryRepository(DomainEventBus domainEventBus){
        this.domainEventBus = domainEventBus;
    }

    @Override
    public Optional<MyTag> findById(String mytagId) {
        MyTag mytag = store.get(mytagId);
        if (null == mytag)
            return Optional.empty();

        var found = new MyTag(
                mytag.getBoardId(),
                mytag.getId(),
                mytag.getName(),
                mytag.getColor());
        found.setVersion(mytag.getVersion());
        return Optional.of(found);
    }

    @Override
    public void save(MyTag mytag) {
        requireNotNull("Tag", mytag);

        // TODO: Add optimistic locking support

        store.remove(mytag.getId());
        mytag.setVersion(mytag.getVersion() + mytag.getDomainEventSize());
        store.put(mytag.getId(), mytag);
        domainEventBus.postAll(mytag);
    }

    @Override
    public void delete(MyTag mytag) {
        store.remove(mytag.getId());
        domainEventBus.postAll(mytag);
    }


//    Answer
//    @Override
//    public void save(MyTag mytag) {
//        requireNotNull("Tag", mytag);
//
//        var old = store.get(mytag.getId());
//        if (null != old && old.getVersion() != mytag.getVersion()){
//            throw new RepositorySaveException(RepositorySaveException.OPTIMISTIC_LOCKING_FAILURE);
//        }
//
//        if (null != old)
//            store.remove(mytag.getId());
//
//        store.remove(mytag.getId());
//        mytag.setVersion(mytag.getVersion() + mytag.getDomainEventSize());
//        store.put(mytag.getId(), mytag);
//        domainEventBus.postAll(mytag);
//    }
}
