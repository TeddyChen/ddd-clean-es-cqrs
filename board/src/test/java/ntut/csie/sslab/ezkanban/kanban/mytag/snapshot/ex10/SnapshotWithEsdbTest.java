package ntut.csie.sslab.ezkanban.kanban.mytag.snapshot.ex10;

import ntut.csie.sslab.ddd.adapter.eventbroker.EsdbListener;
import ntut.csie.sslab.ddd.adapter.eventbroker.EsdbPersistentListener;
import ntut.csie.sslab.ddd.adapter.gateway.GoogleEventBusAdapter;
import ntut.csie.sslab.ddd.adapter.repository.EsdbStoreAdapter;
import ntut.csie.sslab.ddd.usecase.DomainEventMapper;
import ntut.csie.sslab.ddd.usecase.MessageDataMapper;
import ntut.csie.sslab.ddd.usecase.RepositorySaveException;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import ntut.csie.sslab.ezkanban.kanban.common.usecase.AllEventsListener;
import ntut.csie.sslab.ezkanban.kanban.tag.entity.Tag;
import ntut.csie.sslab.ezkanban.kanban.tag.usecase.port.out.repository.SnapshottedTagEventSourcingRepository;
import ntut.csie.sslab.ezkanban.kanban.tag.usecase.port.out.repository.TagEventSourcingRepository;
import ntut.csie.sslab.ezkanban.kanban.tag.usecase.port.out.repository.TagRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class SnapshotWithEsdbTest {

    private final static String ESDB_URL = "esdb://127.0.0.1:2113?tls=false";
    private MyTagRepository mytagRepository;

    private SimpleAsyncTaskExecutor executor;
    private GoogleEventBusAdapter eventBus;
    private AllEventsListener allEventsListener;
    public EsdbListener esdbListener;

    @BeforeEach
    public void setUp(){
        MessageDataMapper.setMapper(MyTagEvents.mapper());
        DomainEventMapper.setMapper(MyTagEvents.mapper());

        eventBus = new GoogleEventBusAdapter();

        // EventStoreDB
        mytagRepository = new MyTagEventSourcingRepository(new EsdbStoreAdapter(ESDB_URL));
        esdbListener = new EsdbPersistentListener(ESDB_URL, MyTagEvents.mapper(), eventBus);

        executor = new SimpleAsyncTaskExecutor();
        executor.execute(esdbListener);
        executor.execute(eventBus);

        allEventsListener = Mockito.mock(AllEventsListener.class);
        eventBus.register(allEventsListener);
    }

    @Test
    public void test_Esdb_repository_snapshot() {
        EsdbStoreAdapter esdbStoreAdapter = new EsdbStoreAdapter(ESDB_URL);
        MyTagRepository mytagRepository = new MyTagSnapshottedEventSourcingRepository(
                (MyTagEventSourcingRepository) this.mytagRepository, esdbStoreAdapter);

        MyTag tag = new MyTag(BoardId.valueOf("board-001"),
                "008",
                "my tag",
                "red");
        tag.rename("name 1");
        tag.rename("name 2");
        tag.rename("name 3");
        tag.changeColor("yellow");
        tag.changeColor("white");
        tag.changeColor("black");

        mytagRepository.save(tag);
    }
}
