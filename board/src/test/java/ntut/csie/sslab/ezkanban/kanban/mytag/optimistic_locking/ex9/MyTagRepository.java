package ntut.csie.sslab.ezkanban.kanban.mytag.optimistic_locking.ex9;


import ntut.csie.sslab.ddd.usecase.AbstractRepository;

interface MyTagRepository extends AbstractRepository<MyTag, String> {
}
