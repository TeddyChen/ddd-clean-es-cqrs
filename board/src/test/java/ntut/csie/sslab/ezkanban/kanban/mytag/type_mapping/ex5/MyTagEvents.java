package ntut.csie.sslab.ezkanban.kanban.mytag.type_mapping.ex5;

import ntut.csie.sslab.ddd.entity.DomainEvent;
import ntut.csie.sslab.ddd.entity.DomainEventTypeMapper;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;

import java.util.Date;
import java.util.UUID;

interface MyTagEvents extends DomainEvent {

    BoardId boardId();
    String tagId();

    default String aggregateId(){
        return boardId().id();
    }

    ///////////////////////////////////////////////////////////////
    record MyTagCreated(
            BoardId boardId,
            String tagId,
            String name,
            String color,
            UUID id,
            Date occurredOn
    ) implements MyTagEvents {}

    record MyTagRenamed(
            BoardId boardId,
            String tagId,
            String name,
            UUID id,
            Date occurredOn
    ) implements MyTagEvents {}

    record MyTagColorChanged(
            BoardId boardId,
            String tagId,
            String color,
            UUID id,
            Date occurredOn
    ) implements MyTagEvents {}

    record MyTagDeleted(
            BoardId boardId,
            String tagId,
            String userId,
            UUID id,
            Date occurredOn
    ) implements MyTagEvents {}

    class TypeMapper extends DomainEventTypeMapper.DomainEventTypeMapperImpl {
        public static final String MAPPING_TYPE_PREFIX = "MyTagEvents$";
        public static final String MYTAG_CREATED = MAPPING_TYPE_PREFIX + "MyTagCreated";
        public static final String MYTAG_RENAMED = MAPPING_TYPE_PREFIX + "MyTagRenamed";
        public static final String MYTAG_COLOR_CHANGED = MAPPING_TYPE_PREFIX + "MyTagColorChanged";
        public static final String MYTAG_DELETED = MAPPING_TYPE_PREFIX + "MyTagDeleted";
        private static final DomainEventTypeMapper mapper;
        static {
            mapper = new DomainEventTypeMapperImpl();
            mapper.put(MYTAG_CREATED, MyTagEvents.MyTagCreated.class);
            mapper.put(MYTAG_RENAMED, MyTagEvents.MyTagRenamed.class);
            mapper.put(MYTAG_COLOR_CHANGED, MyTagEvents.MyTagColorChanged.class);
            mapper.put(MYTAG_DELETED, MyTagEvents.MyTagDeleted.class);
        }
        public static DomainEventTypeMapper getInstance(){
            return mapper;
        }
    }

    static DomainEventTypeMapper mapper(){
        return TypeMapper.getInstance();
    }

}
