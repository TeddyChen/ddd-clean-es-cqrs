package ntut.csie.sslab.ezkanban.kanban.mytag.optimistic_locking.ex9;

import ntut.csie.sslab.ddd.adapter.eventbroker.PostgresDomainEventListener;
import ntut.csie.sslab.ddd.adapter.gateway.GoogleEventBusAdapter;
import ntut.csie.sslab.ddd.adapter.repository.EzOutboxStoreAdapter;
import ntut.csie.sslab.ddd.framework.EzOutboxStore;
import ntut.csie.sslab.ddd.framework.ezes.PgMessageDbClient;
import ntut.csie.sslab.ddd.usecase.DomainEventMapper;
import ntut.csie.sslab.ddd.usecase.MessageDataMapper;
import ntut.csie.sslab.ddd.usecase.RepositorySaveException;
import ntut.csie.sslab.ezkanban.kanban.board.entity.BoardId;
import ntut.csie.sslab.ezkanban.kanban.common.usecase.AbstractSpringBootJpaTest;
import ntut.csie.sslab.ezkanban.kanban.common.usecase.AllEventsListener;
import ntut.csie.sslab.ezkanban.kanban.mytag.adapter.out.repository.springboot.MyTagOrmClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.sql.SQLException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class OptimisticLockingWithOutboxTest extends AbstractSpringBootJpaTest {

    @Autowired
    @Qualifier("MyTagPostgresOutboxStoreClient")
    private EzOutboxStore mytagEzOutboxStore;

    private MyTagRepository mytagRepository;

    private SimpleAsyncTaskExecutor executor;
    private GoogleEventBusAdapter eventBus;
    private AllEventsListener allEventsListener;

    @Autowired
    private MyTagOrmClient mytagOrmStoreClient;

    @Autowired
    private PgMessageDbClient pgMessageDbClient;

    @BeforeEach
    public void setUp(){
        MessageDataMapper.setMapper(MyTagEvents.mapper());
        DomainEventMapper.setMapper(MyTagEvents.mapper());

        mytagRepository = new MyTagOutboxRepository(new EzOutboxStoreAdapter(
                new EzOutboxStore(mytagOrmStoreClient, pgMessageDbClient)));

        eventBus = new GoogleEventBusAdapter();
        executor = new SimpleAsyncTaskExecutor();
        executor.execute(eventBus);

        try {
            postgresDomainEventListener = new PostgresDomainEventListener(JDBC_TEST_URL, "postgres", "root", 20, MyTagEvents.mapper(), eventBus);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        executor.execute(postgresDomainEventListener);

        allEventsListener = Mockito.mock(AllEventsListener.class);
        eventBus.register(allEventsListener);
    }


    @Test
    public void optimistic_locking_failure_with_esdb() {
        MyTag mytag = new MyTag(BoardId.create(),
                UUID.randomUUID().toString(),
                "feature",
                "white");
        mytagRepository.save(mytag);

        MyTag mytagV1 = mytagRepository.findById(mytag.getTagId()).get();
        MyTag mytagV2 = mytagRepository.findById(mytag.getTagId()).get();
        mytagV1.rename("story");
        mytagRepository.save(mytagV1);

        try {
            mytagRepository.save(mytagV2);
            fail("Infeasible path");
        } catch (RepositorySaveException e) {
            assertEquals("Optimistic locking failure", e.getMessage());
        }
    }


}
