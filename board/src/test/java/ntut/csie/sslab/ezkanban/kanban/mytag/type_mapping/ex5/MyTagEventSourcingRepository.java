package ntut.csie.sslab.ezkanban.kanban.mytag.type_mapping.ex5;

import ntut.csie.sslab.ddd.usecase.EventStore;
import ntut.csie.sslab.ddd.usecase.GenericEventSourcingRepository;

import java.util.Optional;

class MyTagEventSourcingRepository implements MyTagRepository {
    private final GenericEventSourcingRepository<MyTag> esRepository;
    private final EventStore eventStore;

    public MyTagEventSourcingRepository(EventStore eventStore) {
        this.esRepository = new GenericEventSourcingRepository<>(eventStore, MyTag.class, MyTag.CATEGORY);
        this.eventStore = eventStore;
    }

    @Override
    public Optional<MyTag> findById(String tagId) {
        return esRepository.findById(tagId);
    }

    @Override
    public void save(MyTag tag) {
        esRepository.save(tag);
    }

    @Override
    public void delete(MyTag tag) {
        esRepository.delete(tag);
    }
}
