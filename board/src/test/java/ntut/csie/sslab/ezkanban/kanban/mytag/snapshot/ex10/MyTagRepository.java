package ntut.csie.sslab.ezkanban.kanban.mytag.snapshot.ex10;


import ntut.csie.sslab.ddd.usecase.AbstractRepository;

interface MyTagRepository extends AbstractRepository<MyTag, String> {
}
